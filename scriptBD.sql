
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 04-06-2014 a las 13:07:32
-- Versión del servidor: 5.1.71
-- Versión de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `u182543195_fct`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE IF NOT EXISTS `administrador` (
  `cod_administrador` int(11) NOT NULL AUTO_INCREMENT,
  `nombre y apellidos` char(35) NOT NULL,
  `identificacion` char(40) NOT NULL,
  `clave` char(40) NOT NULL,
  PRIMARY KEY (`cod_administrador`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`cod_administrador`, `nombre y apellidos`, `identificacion`, `clave`) VALUES
(1, 'mario', 'administrador', 'administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE IF NOT EXISTS `alumnos` (
  `cod_alumno` int(20) NOT NULL AUTO_INCREMENT,
  `nombre_apellidos` varchar(35) NOT NULL,
  `identificacion` varchar(30) NOT NULL,
  `clave` varchar(32) NOT NULL,
  `empresa` varchar(30) NOT NULL,
  `telefono` int(18) NOT NULL,
  `tutor` varchar(20) NOT NULL,
  `cod_profesor` int(11) NOT NULL,
  PRIMARY KEY (`cod_alumno`),
  KEY `cod_profesor` (`cod_profesor`),
  KEY `cod_profesor_2` (`cod_profesor`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`cod_alumno`, `nombre_apellidos`, `identificacion`, `clave`, `empresa`, `telefono`, `tutor`, `cod_profesor`) VALUES
(12, 'Adrián Pérez Cros', 'adrian', '8c4205ec33d8f6caeaaaa0c10a14138c', 'NidoApp', 2147483647, 'Luismi', 31);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informes`
--

CREATE TABLE IF NOT EXISTS `informes` (
  `cod_informe` int(20) NOT NULL AUTO_INCREMENT,
  `informe` longtext NOT NULL,
  `valoracion Personal` text NOT NULL,
  `horas` int(20) NOT NULL,
  `fecha` date NOT NULL,
  `cod_alumno` int(20) NOT NULL,
  PRIMARY KEY (`cod_informe`),
  KEY `cod_alumno` (`cod_alumno`),
  KEY `cod_alumno_2` (`cod_alumno`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE IF NOT EXISTS `mensajes` (
  `cod_mensaje` int(20) NOT NULL AUTO_INCREMENT,
  `mensaje` longtext NOT NULL,
  `fecha_ini` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `fecha_creacion` date NOT NULL,
  `cod_alumno` int(20) NOT NULL,
  `cod_profesor` int(20) NOT NULL,
  PRIMARY KEY (`cod_mensaje`),
  KEY `cod_profesor` (`cod_profesor`),
  KEY `cod_alumno_2` (`cod_alumno`),
  KEY `cod_profesor_2` (`cod_profesor`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=111 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesores`
--

CREATE TABLE IF NOT EXISTS `profesores` (
  `cod_profesor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_apellidos` varchar(35) NOT NULL,
  `identificacion` varchar(30) NOT NULL,
  `clave` varchar(32) NOT NULL,
  `ciclo` varchar(30) NOT NULL,
  `telefono` int(18) NOT NULL,
  `centro` varchar(30) NOT NULL,
  PRIMARY KEY (`cod_profesor`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Volcado de datos para la tabla `profesores`
--

INSERT INTO `profesores` (`cod_profesor`, `nombre_apellidos`, `identificacion`, `clave`, `ciclo`, `telefono`, `centro`) VALUES
(31, 'Mario Pérez Cros', 'mario', 'de2f15d014d40b93578d255e6221fd60', 'Mecanica', 978556112, 'Ies Bajo Aragón');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
