**PROGRAMA FCT**
===================

**Gestión de las FCT de los alumnos en prácticas.**
----------------------------------------------------------------------
____________________________________________
**ADRIÁN PÉREZ CROS**

**2º DAW**
____________________________________________


INDICE
======== 
1.	Introducción.
2.	Requisitos funcionales.
3.	Contrato del proyecto.

    3.1.	Presupuesto detallado.
    
    3.2.	Diagrama de tareas.
    
    3.3.	Planificación temporal con hitos.
4.	Análisis y diseño.
	
    4.1.	Diagrama E/R.
    	
    4.2.	Base de datos.
    	
    4.3.	Diseño de la aplicación.
    	
5.	Manual de instalación.	
6.	Manual de usuario.
 	
    6.1.	Bienvenida.
    
    6.2.	Administrador.
    
    6.3.	Profesor.
    
    6.4.	Alumno.
7. Bibliografía.


1. Introducción
==============

El proyecto fin de grado, trata en desarrollar un **programa para el seguimiento de las FCT**,  la intención es poner en práctica muchos conocimientos que se han ido adquiriendo durante todo el Grado Superior de Desarrollo de Aplicaciones Web.

El **objetivo** del programa para el seguimiento de las FCT, es poder llevar un control detallado y de una forma ordenada todo lo que hacen los alumnos que están en las diferentes empresas. Todo esto será online, ya que el programa estará alojado en un servidor.

El **modelo de negocio** seria poder vender este programa a los diferentes centros para que lo puedan usar, o hacer algún programa parecido para cualquier empresa que requiera llevar un seguimiento de trabajo.

El programa estará hecho para **tres tipos de usuarios**: administrador, profesores y alumnos.
El administrador es el que podrá dar de alta y baja a los diferentes profesores de la aplicación.

El profesor podrá dar de alta a sus alumnos y además poder ver todo lo que están haciendo en las empresas. También podrá mandarles mensaje de aviso(tutorías, reuniones...).

Los alumnos podrán y deberán rellenar el formulario diariamente después de su jornada de trabajo y así poder ver todo su progreso durante las FCT.

Introduction
-----------------

The final degree project is to** develop a program to monitor the FCT**, my intention is to implement a lot of knowledge that I have been acquired throughout the Higher Grade Web Application Development.

The **objective** is to control all the data what the students are in different companies.

All this will be online, as the program will be hosted on a server.

The **business model** will be able to sell this program to the various centers, or make changes in the program to adapt it to another company that requires keeping track of their workers.

The program will be composed of **three types of users**: administrator, teachers and students.

The administrator can enter or delete the different teachers in the application.

Now, the teacher can introduce their students and he can see everything they are doing in his FCT. The teacher can send them warning messages, for example tutorials , meetings, etc. .

Finally, students should complete the form after their daily shift.



2. Requisitos funcionales
========================

- Tendrá un **login**, donde cada unos de los usuarios de la aplicación podrán acceder con su usuario y contraseña.

- Cada **usuario** tendrá su propio panel de administración.

- El **administrador** de la aplicación podrá: dar de alta, baja y listar los profesores.

- Los **profesores** podrán: ver los informes y mandar mensajes a los alumnos, también podrán llevar un mantenimiento de los alumnos (alta, baja, informes pendientes y listarlos), y podrán cambiarse la clave de acceso.

- Los **alumnos** podrán: realizar el informe y cambiar su clave de acceso.

- Las **contraseñas** deberán estar cifradas, una vez registrados se les dará la clave de acceso que luego podrán cambiar.

- El **diseño de la aplicación** deberá ser responsivo tanto para móviles como para tabletas.

3.	Contrato del proyecto.
=========================

Para realizar el proyecto no se precisan de herramientas de pago, solamente incluiré el gasto económico del trabajador. En nuestro caso he establecido un coste de 6€/h.

Como se refleja en la imagen inferior , el tiempo total que se precisa es de 64h * 6€/h = 384€
El coste económico de trabajo es de 384 €.

3.1.	Presupuesto detallado.
------------------------------

Aquí podemos ver detalladamente el día de comienzo del proyecto, así como la finalización del mismo.

Podemos ver la duración del proyecto y el coste del programa.


![presupuesto.png](images/presupuesto.png "")

3.2.	Diagrama de tareas.
---------------------------

Aquí podemos ver todas las tareas detalladas, la duración de la tarea( número de días y la fecha que empieza y acaba esa tarea).

 ![tareas.png](images/tareas.png "")
 
3.3.	Planificación temporal con hitos.
------------------------------------------

Para terminar el proyecto se precisan de 22 días. El horario de cada día es de 3 horas de media. El total de horas destinadas al proyecto son: 64 horas.

En la siguiente imagen se puede ver los días que cuesta realizar cada tarea.

![hitos.png](images/hitos.png "")

4.	Análisis y diseño.
=======================

4.1.	Diagrama E/R.
----------------------

Es una herramienta para el modelado de datos, donde podemos ver las principales entidades así como los atributos que la componen, cada entidad tiene una clave principal la cual será con la que se identifique cada una.

![entidad.png](images/entidad.png "")

4.2.	Base de datos.
-----------------------

La base de datos esta echa en "phpMyAdmin", aquí podemos ver la base de datos con las tablas que la componen y también veremos los atributos de cada una de ellas.

**Base de datos con sus tablas:**
![basedatos.png](images/basedatos.png "")
**Tabla alumnos:**
![tabla alumnos.png](images/tabla alumnos.png "")
**Tabla informes:**
![tablainformes.png](images/tablainformes.png "")
**Tabla mensajes:**
![tablamensajes.png](images/tablamensajes.png "")
**Tabla profesores:**
![tablaprofesores.png](images/tablaprofesores.png "")
**Tabla administrador:**
![tablaadmin.png](images/tablaadmin.png "")

4.3.	Diseño de la aplicación.
--------------------------------
La **aplicación** se dividirá en varias pantallas, aquí veremos algunos diseños.

**Pantalla de login:** esta pantalla será la principal donde cada uno de los alumnos y profesores, con su usuario y contraseña podrán acceder a la aplicación.

![pantallalogin.png](images/pantallalogin.png "")

**Pantalla del profesor:** Cuando entres como profesor veras la siguiente pantalla:

![pantallaprofesor.png](images/pantallaprofesor.png "")

**Pantalla de los alumnos:** Cuando entres como alumno veras la siguiente pantalla:

![pantallaalumnos.png](images/pantallaalumnos.png "")

**Pantalla del administrador**: aquí veras el panel del administrador:

![pantallaadministrador.png](images/pantallaadministrador.png "")

5.	Manual de instalación.
==========================

En el manual de instalación veremos cómo poner en marcha la aplicación.

Lo primero que hay que hacer es buscar donde alojarla.
Yo para alojarla he buscado un servidor gratuito llamado **Hostinger**.

Cuando accedes a la página de Hostinger aparecerá una pantalla para que te logues:

![foto1inst.png](images/foto1inst.png "")

Una vez dentro del hosting, lo primero que hay que hacer es seleccionar el **tipo de hosting**, en este caso seleccionamos uno **gratuito**:

![foto2inst.png](images/foto2inst.png "")

Seguidamente después de elegir el tipo de hosting, pasamos a **configurarlo**:

Creamos el **dominio** o **subdominio**, en este caso como es gratuito solo deja crear un subdominio. Y generamos una contraseña para ese dominio.

![foto3inst.png](images/foto3inst.png "")

Una vez creado y configurado el dominio pasaremos a **crear la base de datos** que nos hace falta para nuestro programa:

Aquí asignaremos nombre a  la base de datos y el usuario perteneciente a esa base de datos.

![foto4inst.png](images/foto4inst.png "")

Una vez creada la base de datos nos saldrán estas **opciones**:

![foto5inst.png](images/foto5inst.png "")


Para **importar la base de datos** que ya tenemos preparada para el funcionamiento de la aplicación, pincharemos sobre la opción de "**phpMyAdmin**".

Una vez dentro del "phpMyAdmin" **importaremos** la base de datos:

![foto6inst.png](images/foto6inst.png "")

Aquí podemos ver que se ha importado correctamente: vemos la **tablas**.

![foto7inst.png](images/foto7inst.png "")

Para poder **subir los archivos al servidor necesitamos los datos del ftp**, para esto vamos al menú del ftp y podremos ver todos los datos correspondientes:

![foto8inst.png](images/foto8inst.png "")

Una vez que tenemos los datos de acceso solo nos hace falta un **programa** para subirlos al servidor, podemos usar **Filezilla**.

Par usar Filezilla necesitamos: **nombre servidor, nombre de usuario y contraseña. **

Cuando hayamos rellenado correctamente esos datos se **mostraran** todos los archivos que tenemos alojados en este servidor.

Aquí podemos descargar, eliminar o añadir archivos cuando queramos.

![foto9inst.png](images/foto9inst.png "")

Con todos estos pasos ya tendremos la **aplicación en funcionamiento.**

Para **comprobarlo** vamos al navegador y escribimos la dirección de subdominio que hemos contratado. Aquí podemos ver que funciona correctamente:

![foto10inst.png](images/foto10inst.png "")

6.	 Manual de usuario.
===================

6.1.	Bienvenida.
----------------------
Esta es la **pantalla inicial** y se muestra más arrancar la aplicación.

Esta pantalla contiene un formulario donde cada usuario con su usuario y contraseña lo rellenara para entrar a la aplicación.

![login.png](images/login.png "")

Una vez rellenado el formulario pulsaremos sobre el botón **entrar**.

Puedes entrar con diferentes tipos de usuario, cada uno tendrá un menú de navegación distinto así como una pantalla de inicio diferente.

Los tres **tipos de usuario** son: **administrador**, **profesor** o **alumno**.

6.2.	Administrador.
--------------------------

Si has entrado a la aplicación como usuario administrador te aparecerá un mensaje de bienvenida y también aparecerá en la parte superior un menú de navegación:

 En esta imagen vemos las diferentes opciones y cosas que puede realizar un administrador.


![menuadmin.png](images/menuadmin.png "")

**Inicio**: Aquí saldrá el mensaje de bienvenida

![iniadmin.png](images/iniadmin.png "")

**Alta profesores:** Aquí saldrá un formulario donde rellenaras todos los campos con los datos del profesor que vayas a insertar.

![altaprofesor.png](images/altaprofesor.png "")

No puedes dejarte ningún campo vacío porque sino saldrá un mensaje de aviso.

![maviso.png](C:/Users/Adrian/Desktop/tutorial/images/maviso.png "")

Una vez que todo está rellenado y le damos al botón insertar y nos saldrá un mensaje en verde si se ha insertado correctamente.

**Baja profesores:** Aquí saldrá una lista con todos los profesores que estén registrados en la aplicación. Donde a la derecha de la tabla aparecerá un botón de eliminar, si pulsamos el botón automáticamente eliminara ese profesor y nos saldrá un mensaje de confirmación.

![bajaprofesores.png](images/bajaprofesores.png "")

**Listar  profesores:** Aquí saldrá una lista con todos los profesores que estén registrados en la aplicación. 

![listaprofesores.png](images/listaprofesores.png "")

6.3.	Profesor.
-------------------
Si has entrado a la aplicación como **usuario profesor** te aparecerá un **mensaje de bienvenida** y también aparecerá en la parte superior un **menú de navegación:**
 En esta imagen vemos las diferentes opciones y cosas que puede realizar un profesor.

![mprofesor.png](images/mprofesor.png "")


**Inicio**: Aquí saldrá el mensaje de bienvenida: donde aparecerá el nombre del profesor, al ciclo y al centro que pertenece.
 
![iniprofesor.png](images/iniprofesor.png "")

**Consulta actividades:** Aquí podremos consultar los informes insertados por los usuarios. 

Saldrá un formulario donde podremos filtrar por el nombre de usuario y por fechas.
 
![c_actividades.png](images/c_actividades.png "")

Cuando hayamos rellenado todos los campos pulsaremos sobre el botón de listar informes, y así nos aparecerá una lista con todos los informes del usuario seleccionado.

**Enviar mensajes**: En esta opción nos saldrá un formulario donde podemos mandar un mensaje a cualquier alumno o también tenemos la opción de mandárselo a todos los alumnos a la vez. 

Tiene dos campos de fecha, uno es el día que quieres que comience el mensaje y el otro el día que desaparezca. 

Estos mensajes se les mostrarán al alumno durante el tiempo elegido en la pantalla del alumno.
 
![enviar_mensajes.png](images/enviar_mensajes.png "")

**Cambio de claves**: Aquí saldrá un formulario donde podrás cambiar la contraseña actual. 

Deberás introducir la contraseña actual correctamente, y la nueva contraseña la deberás escribir bien, para cuando le des al botón "modificar" no salga ningún mensaje de error.

Tipos de mensajes de error: "Error en la contraseña actual" o "las claves no coinciden".

Si todo está bien aparecera un mensaje de "Contraseña modificación con exito".
 
![c_claves.png](images/c_claves.png "")

**Mantenimiento**: En esta opción de menú, se desplegara un submenú con 4 opciones:

    Alta alumnos:  Aquí saldrá un formulario donde rellenaras todos los campos con los datos del alumno que vayas a insertar.

    Ya sabes que no puedes dejar ningún campo vacío sino saltara el mensaje de aviso.
    Una vez le demos al botón de insertar aparecerá el mensaje de todo correcto.
 
   ![alta_alumnos.png](images/alta_alumnos.png "")

	Baja alumnos:  Aquí saldrá una lista con todos los alumnos que estén registrados en la aplicación. Donde a la derecha de la tabla aparecerá un botón de eliminar, si pulsamos el botón automáticamente eliminara ese alumno y nos saldrá un mensaje de confirmación.
 
![baja_alumnos.png](images/baja_alumnos.png "")

	Listado alumnos:  Aquí saldrá una lista con todos los profesores que estén registrados en la aplicación.
	
  ![listado_alumnos.png](images/listado_alumnos.png "")

	Informe Pendiente: esta opción sirve por si algún alumno se olvida de insertar un informe algún día, puede hacerlo mediante un formulario.
	
    Es como la inserción de un informe normal, lo único que eliges el alumno y la fecha en la que se lo ha olvidado.
 
![informe_pendiente.png](images/informe_pendiente.png "")


6.4.	Alumno.
------------------

Si has entrado a la aplicación como **usuario alumno** te aparecerá un **mensaje de bienvenida** y también aparecerá en la parte superior un **menú de navegación**:

 En esta imagen vemos las diferentes opciones y cosas que puede realizar un alumno.
 
![menu_alumno.png](images/menu_alumno.png "")

**Inicio**: Aquí saldrá el mensaje de bienvenida: donde aparecerá el nombre del alumno, la empresa donde está realizando las prácticas y también el tutor de empresa .
 
![inicio_alumno.png](images/inicio_alumno.png "")

**Realizar Informe**: en esta opción nos saldrá un formulario, donde el alumno deberá insertar el informe de lo que ha hecho ese día en la empresa. 
Además del informe, tendrá que poner las horas que ha realizado ese día y una valoración personal.

En la parte superior del formulario tiene la suma de las horas que lleva hasta el momento y la fecha.

Una vez insertado el informe el profesor podrá llevar un control del alumno.
 
![informe.png](images/informe.png "")

Debajo del formulario saldrá un apartado donde aparecerán los mensajes que nos manda el profesor, en caso de que no nos haya mandado mensaje aparecerá vacío.
 
![mensajes.png](images/mensajes.png "")

**Cambios de claves**:  está opción es como la de los profesores, puedes consultarlo en el apartado 6.3.



7. Bibliografía.
=================

**Programas para desarrollo local:**
	
 Wamp: [http://www.wampserver.com/en/](http://www.wampserver.com/en/ "")
 
MySQL Worbench:[ http://www.mysql.com/products/workbench/]( http://www.mysql.com/products/workbench/ "")

**Programa para el diseño Bootstrap:**
			
Bootstrap: [http://getbootstrap.com/](http://getbootstrap.com/ "")
	
**Tutoriales CSS, JavaScript, Jquery,HTML5,PHP:**
		
[http://www.w3schools.com/ ](http://www.w3schools.com/  "")
	
**Tutorial Bootstrap:**			

[http://librosweb.es/bootstrap_3/](http://librosweb.es/bootstrap_3/ "")
	
**Manual PHP:**			

[http://www.php.net/manual/es/](http://www.php.net/manual/es/ "")
	
**Web de soluciones:**				

[http://stackoverflow.com/](http://stackoverflow.com/ "")
	
**Para solucionar case sensitive y el cifrado de las contraseñas:**

[http://stackoverflow.com/questions/21377683/how-to-make-case-sensitive-	log-in-with-my-code](http://stackoverflow.com/questions/21377683/how-to-make-case-sensitive-	log-in-with-my-code "")
	
[http://stackoverflow.com/questions/5235058/password-security-in-php](http://stackoverflow.com/questions/5235058/password-security-in-php "")

**Alojamiento del programa:**

[http://www.hostinger.es/](http://www.hostinger.es/ "")
	 
**Alojamiento de la base de datos:**

[http://www.freemysqlhosting.net/](http://www.freemysqlhosting.net/ "")
