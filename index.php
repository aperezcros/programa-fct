﻿<?php 
	//Para controlar los errores de la base de datos.
	error_reporting(0);
	include("BD.php");
	include("vistas.php");
	mostrar_header();
	//Si es incorreto mostramos el mensaje indicado.
	if (isset($_GET["incorrecto"])){
		mostrarContainer_index("Usuario o contraseña incorrectos");
	}
	//Control de las sesiones de cada uno de los usuarios registrados.
	elseif(isset($_GET["nop"])){
		mostrarContainer_index("No tienes permisos para ver esta página profesor");
	}
	elseif(isset($_GET["noa"])){
		mostrarContainer_index("No tienes permisos para ver esta página alumno");
	}
	elseif(isset($_GET["noad"])){
		mostrarContainer_index("No tienes permisos para ver esta página administrador");
	}
	else{
		mostrarContainer_index();
	}
	mostrar_footer();
?>  
   