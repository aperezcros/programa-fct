﻿<?php 
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["profesor"])){
		header("location: index.php?nop");
	}
	mostrar_header();
	mostrarmenu_profesor();
	mostrarPantalla_profesor($_SESSION["profesor"],$_SESSION['cicloprofesor'],$_SESSION['centroprofesor']);
	mostrar_footer();
?>
