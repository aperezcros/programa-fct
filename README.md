# **Programa FCT** #

Aquí encontraremos todo el código necesario para poner en funcionamiento esta aplicación.

Se divide en:

1.  **Archivos PHP** y además las **carpetas** necesarias(doc,images,videos,js,css).

1.  También hay un archivo **BD.sql**, el cual sirve para importar la base de datos.

1.  En la carpeta **documentación** hay varios documentos:

* Archivo de documentación en **marckdown**.

* Documentación de la aplicación en (html,word,pdf).

* Contrato del proyecto en pdf.

En la documentación veremos los distintos apartados que la componen:

1.	Introducción.
2.	Requisitos funcionales.
3.	Contrato del proyecto.

    3.1.	Presupuesto detallado.
    
    3.2.	Diagrama de tareas.
    
    3.3.	Planificación temporal con hitos.

4.	Análisis y diseño.
	
    4.1.	Diagrama E/R.
    	
    4.2.	Base de datos.
    	
    4.3.	Diseño de la aplicación.
    	
5.	Manual de instalación.	
6.	Manual de usuario.
 	
    6.1.	Bienvenida.
    
    6.2.	Administrador.
    
    6.3.	Profesor.
    
    6.4.	Alumno.

7. Bibliografía.