﻿<?php 
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["administrador"])){
		header("location: index.php?noad");
	}
	mostrar_header();
	mostrarmenu_administrador();
	mostrarPantalla_administrador();
	mostrar_footer();
?>  
   