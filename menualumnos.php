﻿<?php 
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["alumno"])){
		header("location: index.php?noa");
	}
	mostrar_header();
	mostrarmenu_alumno();
	mostrarPantalla_alumno($_SESSION["alumno"],$_SESSION["empresaalumno"],$_SESSION['tutoralmuno']);
	mostrar_footer();
?>
