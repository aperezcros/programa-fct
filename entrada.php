<?php 
	//Para controlar los errores de la base de datos.
	//error_reporting(0);
	include("BD.php");
	session_start();
	$usu=$_POST["usuario"];
	$con=$_POST["contrasena"];
	//Comprobar a que rango pertenece el usuario que ha iniciado en la aplicación.
	//Comprobar de si es profesor
	$profesor = obtener_profesor($usu,$con);
		if ($profesor){
				$_SESSION['profesor']=$profesor['identificacion'];
				$_SESSION['contrasenaprofesor']=$profesor['clave'];
				$_SESSION['cicloprofesor']=$profesor['ciclo'];
				$_SESSION['centroprofesor']=$profesor['centro'];
				$_SESSION['cod_profesor']=$profesor['cod_profesor'];
				//header("location: menuprofesores.php");
				echo "<script> location.href='menuprofesores.php'</script>";
		}
		else{
			//Comprobar si es alumno
			$alumno = obtener_alumno($usu,$con);
			if ($alumno){
				$_SESSION['alumno']=$alumno['identificacion'];
				$_SESSION['contrasenaalumno']=$alumno['clave'];
				$_SESSION['empresaalumno']=$alumno['empresa'];
				$_SESSION['tutoralmuno']=$alumno['tutor'];
				$_SESSION['cod_alumno']=$alumno['cod_alumno'];
				//header("location: menualumnos.php");
				echo "<script> location.href='menualumnos.php'</script>";
			}
			else{
			//Comprobar si es administrador
				$admin = obtener_administrador($usu,$con);
				if ($admin){
					$_SESSION['administrador']=$admin['identificacion'];
					$_SESSION['contrasenaadministrador']=$admin['clave'];
					$_SESSION['cod_administrador']=$admin['cod_administrador'];
					//header("location: menuadministrador.php");
					echo "<script> location.href='menuadministrador.php'</script>";
				}
				else{
				//Usuario y contraseña invalidos
					//header("location: index.php?incorrecto");
					echo "<script> location.href='index.php?incorrecto'</script>";
				}
				
			}
		}	
?>
