﻿<?php 
	//Para controlar los errores de la base de datos.
	error_reporting(0);
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["administrador"])){
		header("location: index.php?noad");
	}
	//Comprobamos si se a eliminado
	$mensaje="";
	if(isset($_GET['eliminar'])){
		$codigo_profesor=$_GET['profesor'];
		if(eliminar_profesor($codigo_profesor)){
			$mensaje="<span style='color:green'>Datos eliminados</span>";
		}
		else{
			$mensaje="Error, no se ha podido eliminar";
		}
	}
	//Obtenemos los profesores
	$profesores=obtener_profesores();
	//Pintamos la pagina
	mostrar_header();
	mostrarmenu_administrador();
	?>
	<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Dar de baja profesores</h1>
					<center><p style=color:red> <?php  echo $mensaje;?></p></center>
				<section>
					<?php
					//Listamos los profesores con el boton de eliminar a la izquierda
					listar_profesores_baja($profesores);
					?>
				</section>
			</div>
<?php
	mostrar_footer();
?>  
   