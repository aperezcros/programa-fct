﻿<?php 
	//Para controlar los errores de la base de datos.
	error_reporting(0);
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["profesor"])){
		header("location: index.php?nop");
	}
	//Si pulsamos al botón insertar
	if(isset($_GET['insertar']))
						{
						if($_GET['nombre']<>null)
						{
						if($_GET['identificacion']<>null)
						{
						if($_GET['clave']<>null)
						{
						if($_GET['empresa']<>null)
						{
						if($_GET['telefono']<>null)
						{
						if($_GET['tutor']<>null)
						{
						//LLamamos a la funcion insertar y le pasamos los valores recogidos en el formulario.
						if(insertar_alumnos($_GET['nombre'],$_GET['identificacion'],$_GET['clave'],$_GET['empresa'],$_GET['telefono'],$_GET['tutor'],$_SESSION['cod_profesor'])){
							$mensaje = "<span style='color:green'>Datos insertados con exito</span>";
						}
						else{
							$mensaje = "Error, no se ha podido insertar";
						}
						}//null tutor
						}//null telefono
						}//null empresa
						}//null clave
						}//null identificacion
						}// null nombre				
	}
	
	mostrar_header();
	mostrarmenu_profesor();
?>	
	<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Alta alumnos</h1>
				<section>
					<form class="form-horizontal" role="form" method="GET" action="alta_alumnos.php">
						<input type="hidden" name="codigo">	
						<center><p style=color:red> <?php  echo $mensaje;?></p></center>
						  <div class="form-group">
							<label for="nombre" class="col-lg-2 control-label">Nombre y apellidos</label>
							<div class="col-lg-10">
							<input type="text" name="nombre" class="form-control"  placeholder="Introduce tu nombre y apellidos" title="Se necesita nombre y apellidos"required />							
							</div>
						  </div>
						  <div class="form-group">
							<label for="identificacion" class="col-lg-2 control-label">Identificacion</label>
							<div class="col-lg-10">
							  <input type="text" name="identificacion" class="form-control" id="identificacion" 
									 placeholder="Introduce identificacion" title="Se necesita identificacion"required />
							</div>
						  </div>
						  <div class="form-group">
							<label for="clave" class="col-lg-2 control-label">Clave</label>
							<div class="col-lg-10">
							  <input type="password" name="clave" class="form-control" id="clave" 
									 placeholder="Introduce clave" title="Se necesita clave"required />
							</div>
						  </div>
						  <div class="form-group">
							<label for="empresa" class="col-lg-2 control-label">Empresa</label>
							<div class="col-lg-10">
							  <input type="text" name="empresa" class="form-control" id="ciclo" 
									 placeholder="Introduce empresa" title="Se necesita empresa" required />
							</div>
						  </div>
						  <div class="form-group">
							<label for="telefono" class="col-lg-2 control-label">Telefono</label>
							<div class="col-lg-10">
							  <input type="number" name="telefono" class="form-control" id="telefono" 
									 placeholder="Introduce telefono" title="Se necesita telefono"required />
							</div>
						  </div>
						  <div class="form-group">
							<label for="tutor" class="col-lg-2 control-label">Tutor</label>
							<div class="col-lg-10">
							  <input type="text" name="tutor" class="form-control" id="centro" 
									 placeholder="Introduce tutor" title="Se necesita tutor"required />
							</div>
						  </div>
						  <div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
							  <button type="submit" name="insertar" class="btn btn-default" id="insertar">Insertar</button>
							</div>
						  </div>
					</form>
				</section>
			</div>
<?php
	mostrar_footer();
?>
