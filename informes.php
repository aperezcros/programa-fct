﻿<?php 
	//Para controlar los errores de la base de datos.
	error_reporting(0);
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["profesor"])){
		header("location: index.php?nop");
	}
	//Si ha apretado el botón insertar llamaremos a la funcion insertar informe.
	if(isset($_GET['insertar']))
						{
						if($_GET['informe']<>null)
						{
						if($_GET['valoracion_personal']<>null)
						{
						if($_GET['horas']<>null)
						{
						if(insertar_informes($_GET['informe'],$_GET['valoracion_personal'],$_GET['horas'],$_GET['fecha'],$_GET['seleccion'])){
							$mensaje = "<span style='color:green'>Datos insertados con exito</span>";
						}
						else{
							$mensaje = "Error, no se ha podido insertar";
						}
						}//null horas
						}//null valoracion personal
						}// null informe
	}// fin del if isset
	
	mostrar_header();
	mostrarmenu_profesor();
?>	
	<!--Container -->
			<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Insertar informe</h1>
				<section>
					<form class="form-horizontal" role="form" method="GET" action="informes.php">
						  <center><p style=color:red> <?php  echo $mensaje;?></p></center>
						  <div class="form-group">
							<label for="seleccion" class="col-lg-2 control-label">Nombre Alumno</label>
							<div class="col-lg-10">
								<select name="seleccion" class="form-control" placeholder="Selecciona alumno" title="Se necesita que selecciones un alumno" required>
								<?php
									$sql="SELECT DISTINCT cod_alumno,nombre_apellidos FROM alumnos";
									$resultado=mysql_query($sql,conexion());
									while($fila=mysql_fetch_array($resultado)){
									$cod_alumno=$fila[0];
									echo "<option value='".$cod_alumno."'>".$fila[1]."</option>";
									//echo "<option selected='selected'></option>";
									}
								?>
								</select>
							</div>
						  </div>
						  <div class="form-group">
							<label for="informe" class="col-lg-2 control-label">Informe</label>
							<div class="col-lg-10">
							<textarea rows="5" cols="90" type="text" name="informe" class="form-control"  placeholder="Introduce tu informe" title="Se necesita que insertes el informe"required></textarea>							
							</div>
						  </div>
						  <div class="form-group">
							<label for="valoracion_personal" class="col-lg-2 control-label">Valoración Personal</label>
							<div class="col-lg-10">
							<input type="text" name="valoracion_personal" class="form-control"  placeholder="Introduce valoracion" title="Se necesita que insertes tu valoracion" required></textarea>							
							</div>
						  </div>
						  <div class="form-group">
							<label for="horas" class="col-lg-2 control-label">Horas</label>
							<div class="col-lg-10">
							<input type="number" name="horas" class="form-control"  placeholder="Introduce horas" title="Se necesita que insertes las horas realizadas" required></textarea>							
							</div>
						  </div>
						  <?php $hoy = date("Y-m-d"); ?> 
						  <div class="form-group">
							<label for="fecha" class="col-lg-2 control-label">Fecha pendiente</label>
							<div class="col-lg-10">
							<input type="text" name="fecha" class="form-control"  placeholder="Introduce fecha" title="Se necesita que insertes fecha" required></textarea>							
							</div>
						  </div>
						  <div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
							  <button type="submit" name="insertar" class="btn btn-default" id="insertar">Insertar</button>
							</div>
						  </div>
						 
					</form>
				</section>
		</div>
<?php
	mostrar_footer();
?>
