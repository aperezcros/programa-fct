<?php 
	session_start();
	session_destroy();
	unset($_SESSION['usuario']);
	unset($_SESSION['contrasena']);
	header("location: index.php");
?>