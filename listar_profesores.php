﻿<?php 
	//Para controlar los errores de la base de datos.
	error_reporting(0);
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["administrador"])){
		header("location: index.php?noad");
	}
	//Array de profesores
	$profesores = obtener_profesores();
	/*Comprobacion de codigo
	echo "<pre>";
	print_r($profesores);
	echo "</pre>";
	*/
	mostrar_header();
	mostrarmenu_administrador();
	?>
	<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Listado de los profesores</h1>
				<section>
				<?php
					//Listamos los profesores
					listar_profesores($profesores);
				?>
				</section>
			</div>
<?php
	mostrar_footer();
?>  
   