﻿<?php
	//Mostrar el header 
	function mostrar_header(){
?>
		<!DOCTYPE HTML>
		<html lang="es">
		<head>
			<meta charset="UTF-8">
			<title>Seguimiento FCT</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
			<link href='http://fonts.googleapis.com/css?family=Nova+Square' rel='stylesheet' type='text/css'>
			<script language='javascript' src="popcalendar.js"></script>
		</head>		
		<body>
		<!--Encabezado de la pagina -->
		<header>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h1 class="texto tamtex1">CPIFP LOS ENLACES</h1>
						<p><a href="doc/ManualUsuario.pdf" id="doc">Ayuda</a></p>
						<p><a href="videos.php" id="doc">VideoTutoriales</a></p>
						
					</div>
				</div>
			</div>
		</header>
	
<?php	
	}
	
	//Mostrar el container del index con la parte del login
	function mostrarContainer_index($mensaje=""){
?>	
		<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Accede al seguimiento</h1>
				<section>
					<form class="form-horizontal" role="form" method="post" action="entrada.php">
						  <div class="form-group">
							<label for="usuario" class="col-lg-2 control-label">Usuario</label>
							<div class="col-lg-10">
							  <input type="text" name="usuario" class="form-control" id="usuario" placeholder="Introduce Usuario" title="Se necesita nombre de usuario"required />
							</div>
						  </div>
						  <div class="form-group">
							<label for="contrasena" class="col-lg-2 control-label">Contraseña</label>
							<div class="col-lg-10">
							  <input type="password" name="contrasena" class="form-control" id="contrasena" 
									 placeholder="Introduce contraseña" title="Se necesita contraseña"required />
							</div>
						  </div>
						  <div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
							  <button type="submit" name="entrar" class="btn btn-default" id="botonenviar">Entrar</button>
							</div>
						  </div>
					</form>
				
				<center><p style=color:red> <?php  echo $mensaje;?></p></center>
				</section>
				<section>
					<div class="row">
					</div>
					<br><br>
					<div class="row">
						<div class="col-xs-10 col-md-8 col-lg-8  col-xs-offset-1 col-md-offset-2 col-lg-offset-2">
						<p>Este programa sirve para llevar un seguimiento controlado de los alumnos en las FCT.</p>
						</div>
					</div>
				</section>
			</div>
<?php	
	}
	
	//Mostrar el footer de la página
	function mostrar_footer(){
?>
		<!--Footer --> 
		<footer class="colorfooter">
			
			   <section class="colorseccion">
					<div class="container">
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<h3>Seguimiento FCT</h3>
								<p>Diseñador por: Adrián Pérez Cros.</p>
								<p><a href="https://bitbucket.org/aperezcros/programa-fct">Repositorio</a></p>
							</div>
						  
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<h3>Siguenos en:</h3>
								<a href='#' class='boton_rs_twitter' title='twitter'></a>
								<a href='#' class='boton_rs_facebook' title='facebook'></a>
								<a href='#' class='boton_rs_rss' title='rss'></a>
							</div>
						</div>
					</div> 
				
			   </section>
		</footer>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
	</body>
	</html>
<?php
	}
	//Mostrar el menu administrador.
	function mostrarmenu_administrador(){
?>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav" id="menu">
					<li><a href="menuadministrador.php">Inicio</a></li>
                    <li><a href="alta_profesores.php">Alta Profesores</a></li>
                    <li><a href="baja_profesores.php">Baja Profesores</a></li>
                    <li><a href="listar_profesores.php">Listar Profesores</a></li>
                    <li><a href="cerrar.php">Salir</a></li>
                </ul>
            </div>
        </div>
    </nav>
<?php
	}
	
	//Mostrar la pantalla que saldra cuando nos hayamos registrador con el usuario administrador.
	function mostrarPantalla_administrador(){
?>
	<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Bienvenido al panel de control del Administrador.</h1>
				<section>
					<div class="row">
						<div class="col-xs-10 col-md-8 col-lg-8  col-xs-offset-1 col-md-offset-2 col-lg-offset-2">
						<p>Podras llevar una gestión de todos los profesores del centro, donde podras darlos de alta y baja.</p>
						</div>
					</div>
				</section>
			</div>
<?php
	}
	//Mostrar menu alumno
	function mostrarmenu_alumno(){
?>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav" id="menu">
				    <li><a href="menualumnos.php">Inicio</a></li>
                    <li><a href="insertar_informes.php">Realizar informe</a></li>
                    <li><a href="cambio_claves.php">Cambio de claves</a></li>
                    <li><a href="cerrar.php">Salir</a></li>
                </ul>
            </div>
        </div>
    </nav>
<?php
	}
	
	//Mostrar la pantalla que saldra cuando nos hayamos registrador con el usuario alumno.
	function mostrarPantalla_alumno($nombrealumno,$empresaalumno,$tutor){
?>

	<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Bienvenido al panel de control de los alumnos.</h1>
				<section>
					<div class="row">
						<div class="col-xs-10 col-md-8 col-lg-8  col-xs-offset-1 col-md-offset-2 col-lg-offset-2">
						<p>Podras llevar una gestión de tus informes, además de poder cambiar la clave de acceso.</p>
						</div>
					</div>
				</section>
					<section>
					<div class="row">
							<div class="col-xs-10 col-md-8 col-lg-8  col-xs-offset-1 col-md-offset-2 col-lg-offset-2">
							<h2>Datos Personales</h2>
							<p style="font-weight:bold;">Nombre:<span style=color:red><?php echo $nombrealumno ?></span></p>
							<p style="font-weight:bold;">Empresa:<span style=color:red><?php echo $empresaalumno ?></span></p>
							<p style="font-weight:bold;">Tutor:<span style=color:red><?php echo $tutor ?></span></p>
							<img src="img/perfil.jpg"/>
							</div>
					</div>
					<br>
				</section>
			</div>
<?php
	}
	
	//Mostrar menu profesor
	function mostrarmenu_profesor(){
?>
	<nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav" id="menu">
				    <li><a href="menuprofesores.php">Inicio</a></li>
                    <li><a href="consulta_actividades.php">Consulta actividades</a></li>
					<li><a href="enviar_mensajes.php">Enviar mensajes</a></li>
					<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Mantenimiento
					<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="alta_alumnos.php">Alta alumnos</a></li>
						<li><a href="baja_alumnos.php">Baja alumnos</a></li>
						<!--<li><a href="modificaciones.php">Modificaciones alumnos</a></li>-->
						<li><a href="listar_alumnos.php">Lista de alumnos</a></li>
						<li><a href="informes.php">Informes Pendientes</a></li>
					</ul>
					</li>
                    <li><a href="cambio_claves_profesor.php">Cambio de claves</a></li>
                    <li><a href="cerrar.php">Salir</a></li>
                </ul>
            </div>
        </div>
    </nav>

<?php
	}
	
	//Mostrar la pantalla que saldra cuando nos hayamos registrador con el usuario profesor.
	function mostrarPantalla_profesor($nombreprofesor,$ciclo,$centro){
?>
	<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Bienvenido al panel de control de los profesores.</h1>
				<section>
					<div class="row">
						<div class="col-xs-10 col-md-8 col-lg-8  col-xs-offset-1 col-md-offset-2 col-lg-offset-2">
							<p>Podras llevar una gestión de tus informes que ha realizado cada alumno, 
							podras además mandar mensajes de aviso. Y podras llevar un mantenimiento de todos los alumnos.</p>
						</div>
					</div>
				</section>
					<section>
					<div class="row">
							<div class="col-xs-10 col-md-8 col-lg-8  col-xs-offset-1 col-md-offset-2 col-lg-offset-2">
							<h2>Datos Personales</h2>
							<p style="font-weight:bold;">Nombre:<span style=color:red><?php echo $nombreprofesor ?></span></p>
							<p style="font-weight:bold;">Ciclo:<span style=color:red><?php echo $ciclo ?></span></p>
							<p style="font-weight:bold;">Centro:<span style=color:red><?php echo $centro ?></span></p>
							<img src="img/perfil.jpg"/>
							</div>
					</div>
					<br>
				</section>
			</div>
<?php
	}
	
//Listar todos los profesores
	function listar_profesores($profesores){
?>	<div class=\"table-responsive\">
		<center><table border = '1' class='table'>  
		<tr style=color:red><td>Nombre y apellidos</td><td>Identificacion</td><td>Clave</td><td>Ciclo</td><td>Telefono</td><td>Centro</td></tr>
<?php		foreach($profesores as $fila){?>
			<tr>
			<td>
			<center><?php echo $fila[1];?></center>
			</td>
			<td>
			<center><?php echo $fila[2];?></center>
			</td>
			<td>
			<center><?php echo $fila[3];?></center>
			</td>
			<td>
			<center><?php echo $fila[4];?></center>
			</td>
			<td>
			<center><?php echo $fila[5];?></center>
			</td>
			<td>
			<center><?php echo $fila[6];?></center>
			</td>
			</tr>
		<?php } ?>
		</table></center>
		</div>
<?php		
	}

	//Listar todos los profesores con boton baja
	function listar_profesores_baja($profesores){
?>	
		<div class="table-responsive">
		<center><table border = '1' class='table'>  
		<tr style="color:red"><td>Nombre y apellidos</td><td>Identificacion</td><td>Clave</td><td>Ciclo</td><td>Telefono</td><td>Centro</td></tr>
<?php		foreach($profesores as $fila){?>
			<tr>
			<td>
			<center><?php echo $fila[1];?></center>
			</td>
			<td>
			<center><?php echo $fila[2];?></center>
			</td>
			<td>
			<center><?php echo $fila[3];?></center>
			</td>
			<td>
			<center><?php echo $fila[4];?></center>
			</td>
			<td>
			<center><?php echo $fila[5];?></center>
			</td>
			<td>
			<center><?php echo $fila[6];?></center>
			</td>
			<td>
			<form method="get" action="baja_profesores.php">
				<input type="hidden" name="profesor" value="<?php echo $fila[0]; ?>">
				<input type="submit" name="eliminar" value="eliminar" onclick="return confirm('Vas a  eliminar este profesor');\">
				</form>
			</td>
			</tr>
		<?php } ?>
		</table></center>
		</div>
<?php		
	}
	
	//Listar mensajes
	function listar_mensajes($mensaje){
?>	
	<center><h2>MENSAJES RECIBIDOS</h2></center>
	<?php foreach($mensaje as $fila){ ?>
	<center><?php echo $fila[0]; ?></center>
<?php
		}
	}
	
	//Listar informes
	function listar_informes($informes){
?>	
	<div class="table-responsive">
	<table border = '1' class="table">  
	<tr style=color:red><td>Codigo</td><td>Informe</td><td>Valoracion</td><td>Horas</td><td>Fecha</td><td>Cod_alumno</td></tr>
	<?php foreach($informes as $fila){ ?>
		<tr><td>
		<?php echo $fila[0]; ?>
		</td><td>
		<?php echo $fila[1]; ?>
		</td><td>
		<?php echo $fila[2]; ?>
		</td><td>
		<?php echo $fila[3]; ?>
		</td><td>
		<?php echo $fila[4]; ?>
		</td><td>
		<?php echo $fila[5]; ?>
		</td></tr>
<?php
		}
	echo "</table>";
	echo "</div>";
	}
	//Listar todos los alumnos con boton baja
	function listar_alumnos_baja($alumnos){
?>
<div class="table-responsive">
		<center><table border = '1' class='table'>  
		<tr style="color:red"><td>Nombre y apellidos</td><td>Identificacion</td><td>Clave</td><td>Empresa</td><td>Telefono</td><td>Tutor</td></tr>
<?php		foreach($alumnos as $fila){?>
			<tr>
			<td>
			<center><?php echo $fila[1];?></center>
			</td>
			<td>
			<center><?php echo $fila[2];?></center>
			</td>
			<td>
			<center><?php echo $fila[3];?></center>
			</td>
			<td>
			<center><?php echo $fila[4];?></center>
			</td>
			<td>
			<center><?php echo $fila[5];?></center>
			</td>
			<td>
			<center><?php echo $fila[6];?></center>
			</td>
			<td>
			<form method="get" action="baja_alumnos.php">
				<input type="hidden" name="alumno" value="<?php echo $fila[0]; ?>">
				<input type="submit" name="eliminar" value="eliminar" onclick="return confirm('Vas a  eliminar este alumno');\">
				</form>
			</td>
			</tr>
		<?php } ?>
		</table></center>
		</div>
<?php 
}
	//Listar todos los alumnos
	function listar_alumnos($alumnos){
?>
<div class="table-responsive">
		<center><table border = '1' class='table'>  
		<tr style="color:red"><td>Nombre y apellidos</td><td>Identificacion</td><td>Clave</td><td>Empresa</td><td>Telefono</td><td>Tutor</td></tr>
<?php		foreach($alumnos as $fila){?>
			<tr>
			<td>
			<center><?php echo $fila[1];?></center>
			</td>
			<td>
			<center><?php echo $fila[2];?></center>
			</td>
			<td>
			<center><?php echo $fila[3];?></center>
			</td>
			<td>
			<center><?php echo $fila[4];?></center>
			</td>
			<td>
			<center><?php echo $fila[5];?></center>
			</td>
			<td>
			<center><?php echo $fila[6];?></center>
			</td>
			</tr>
		<?php } ?>
		</table></center>
		</div>
<?php 
}
	
	//Funcion para mostrar la pantalla de los videos
	function mostrarPantalla_videos(){
?>
<!--Container -->
	<div class="container" style="min-height:500px;">
		<br>
		<h1 id="encabezado">Tutoriales</h1>
		<section>
			<div class="row">
				<div class="col-md-4 col-xs-12 col-lg-4">
					<p>Gestion del administrador</p>
							<video video controls class="img img-responsive" width="360" height="249">
									<source src="videos/administrador.mp4" type="video/mp4">
							</video>           
				</div>
				<div class="col-md-4 col-xs-12 col-lg-4">
					<p>Gestion del profesor</p>
							<video video controls class="img img-responsive" width="360" height="249">
									<source src="videos/profesor.mp4" type="video/mp4">
							</video>           
				</div>
				<div class="col-md-4 col-xs-12 col-lg-4">
					<p>Gestion del alumno</p>
							<video video controls class="img img-responsive" width="360" height="249">
									<source src="videos/alumno.mp4" type="video/mp4">
							</video>         
				</div>
			</div>
		</section>
		
	</div>
	<br>
<?php
}
?>