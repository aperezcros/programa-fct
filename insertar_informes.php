﻿<?php 
	//Para controlar los errores de la base de datos.
	error_reporting(0);
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["alumno"])){
		header("location: index.php?noa");
	}
	//Si ha apretado el botón insertar llamaremos a la funcion insertar informe.
	if(isset($_GET['insertar']))
						{
						if($_GET['informe']<>null)
						{
						if($_GET['valoracion_personal']<>null)
						{
						if($_GET['horas']<>null)
						{
						if(insertar_informes($_GET['informe'],$_GET['valoracion_personal'],$_GET['horas'],$_GET['fecha'],$_SESSION['cod_alumno'])){
							$mensaje = "<span style='color:green'>Datos insertados con exito</span>";
						}
						else{
							$mensaje = "Error, no se ha podido insertar";
						}
						}//null horas
						}//null valoracion personal
						}// null informe
	}// fin del if isset
	
	//Array de mensajes
	$hoy = date("Y-m-d");
	$mensajes = obtener_mensaje($_SESSION['cod_alumno'],$hoy);
	
	$horastotales=total_horas($_SESSION['cod_alumno']);
	mostrar_header();
	mostrarmenu_alumno();
?>
	<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Insertar informe</h1>
				<section>
					<form class="form-horizontal" role="form" method="GET" action="insertar_informes.php">
						<input type="hidden" name="codigo"/>
						<center><b>Alumno:&nbsp;</b><?php echo "<h12>".$_SESSION['alumno']."</h12>"?></center>
						<center>&nbsp;<b>Fecha</b>&nbsp;<?php $hora = date("g:i a");?><?php echo "<h12>".$hoy."</h12>&nbsp;"."<h12>".$hora."</h12>&nbsp;" ?><b>Total horas&nbsp;</b><?php echo $horastotales;?></center>
						  <center><p style=color:red> <?php  echo $mensaje;?></p></center>
						  <div class="form-group">
							<label for="informe" class="col-lg-2 control-label">Informe</label>
							<div class="col-lg-10">
							<textarea rows="5" cols="90" type="text" name="informe" class="form-control"  placeholder="Introduce tu informe" title="Se necesita que insertes el informe"required></textarea>							
							</div>
						  </div>
						  <div class="form-group">
							<label for="valoracion_personal" class="col-lg-2 control-label">Valoración Personal</label>
							<div class="col-lg-10">
							<input type="text" name="valoracion_personal" class="form-control"  placeholder="Introduce valoracion" title="Se necesita que insertes tu valoracion" required></textarea>							
							</div>
						  </div>
						  <div class="form-group">
							<label for="horas" class="col-lg-2 control-label">Horas</label>
							<div class="col-lg-10">
							<input type="number" name="horas" class="form-control"  placeholder="Introduce horas" title="Se necesita que insertes las horas realizadas" required></textarea>							
							</div>
						  </div>
						  <?php $hoy = date("Y-m-d"); ?> 
						  <input type="hidden" value="<?php echo "".$hoy."" ?>" name="fecha"/>
						  <div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
							  <button type="submit" name="insertar" class="btn btn-default" id="insertar">Insertar</button>
							</div>
						  </div>
						 
					</form>
				</section>
				<section>
				<?php
				//Listamos los mensajes del usuario que ha entrado
					listar_mensajes($mensajes);
				?>
				</section>
		</div>

<?php
	mostrar_footer();
?>
