﻿<?php 
	//Para controlar los errores de la base de datos.
	error_reporting(0);
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["profesor"])){
		header("location: index.php?nop");
	}
	//Si pulsamos al botón eliminar
		$mensaje="";
			if(isset($_GET['eliminar'])){
				$codigo_alumno=$_GET['alumno'];
				if(eliminar_alumno($codigo_alumno)){
					$mensaje="<span style='color:green'>Datos eliminados</span>";
				}
				else{
					$mensaje="Error, no se ha podido eliminar";
				}
			}
	$alumnos=obtener_alumnos();
	mostrar_header();
	mostrarmenu_profesor();
?>	
	<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Dar de baja alumnos</h1>
					<center><p style=color:red> <?php  echo $mensaje;?></p></center>
				<section>
					<?php
					//Listamos los profesores con el boton de eliminar a la izquierda
					listar_alumnos_baja($alumnos);
					?>
				</section>
			</div>
<?php
	mostrar_footer();
?>
