﻿<?php 
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["profesor"])){
		header("location: index.php?nop");
	}
	$informes="";
	if(isset($_GET['listar'])){
		$seleccion=$_GET['seleccion'];
		$fecha=$_GET['fecha1'];
		$fecha2=$_GET['fecha2'];
		
		$informes=selecciona_informes($seleccion,$fecha,$fecha2);
		
		
	}
	
	mostrar_header();
	mostrarmenu_profesor();
	if($informes!=""){
?>
	<div class="container" style="min-height:500px;">
	<br>
	<h1 id="encabezado">Lista informes</h1>
		<section>
					<?php
						listar_informes($informes);
					?>
		</section>
	</div>	
<?php
	}
	else{
?>
		<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Consulta Actividades</h1>
				<section>
					<form class="form-horizontal" role="form" method="get" action="consulta_actividades.php" name="formulario">
						  <div class="form-group">
							<label for="seleccion" class="col-lg-2 control-label">Nombre Alumno</label>
							<div class="col-lg-10">
								<select name="seleccion" class="form-control" placeholder="Selecciona alumno" title="Se necesita que selecciones un alumno" required>
								<?php
									$sql="SELECT DISTINCT i.cod_alumno,nombre_apellidos FROM alumnos a, informes i WHERE a.cod_alumno=i.cod_alumno";
									$resultado=mysql_query($sql,conexion());
									while($fila=mysql_fetch_array($resultado)){
									$cod_alumno=$fila[0];
									echo "<option value='".$cod_alumno."'>".$fila[1]."</option>";
									}
									echo "<option selected='selected'></option>";
								?>
								</select>
							</div>
						  </div>
						  <div class="form-group">
							<label for="fecha1" class="col-lg-2 control-label">Fecha desde</label>
							<div class="col-lg-10">
								<input name="fecha1" type="text" id="fecha" onClick="popUpCalendar(this, formulario.fecha, 'yyyy-mm-dd');" size="20"  class="form-control"  placeholder="Introduce fecha inicio" title="Se necesita que insertes fecha inicio"  required>						
							</div>
						  </div>
						  <div class="form-group">
							<label for="fecha2" class="col-lg-2 control-label">Fecha hasta</label>
							<div class="col-lg-10">
							<input name="fecha2" type="text" id="fecha1" onClick="popUpCalendar(this, formulario.fecha2, 'yyyy-mm-dd');" size="20" class="form-control"  placeholder="Introduce fecha final" title="Se necesita que insertes fecha final" required>							
							</div>
						  </div>
						  <div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
							  <button type="submit" name="listar" class="btn btn-default" id="listar">Listar informes</button>
							</div>
						  </div>
						 
					</form>
				</section>
			</div>
<?php	
	}
	mostrar_footer();
?>
