﻿<?php 
	//Para controlar los errores de la base de datos.
	error_reporting(0);
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["profesor"])){
		header("location: index.php?nop");
	}
	//Si pulsamos al botón insertar
	if(isset($_GET['insertar'])){	
		//Llamamos a la funcion mensajes y le pasamos los datos introducidos en el formulario.
		if(insertar_mensajes($_GET['mensaje'],$_GET['fecha1'],$_GET['fecha2'],$_GET['fechahoy'],$_GET['seleccion'],$_SESSION['cod_profesor'])){
							$mensaje = "<span style='color:green'>Datos insertados con exito</span>";
						}
						else{
							$mensaje = "Error, no se ha podido insertar";
						}
	}
	
	mostrar_header();
	mostrarmenu_profesor();
?>	
	<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Enviar mensaje</h1>
				<section>
				<center><p style=color:red> <?php  echo $mensaje;?></p></center>
					<form class="form-horizontal" role="form" method="get" action="enviar_mensajes.php" name="formulario">
						  <div class="form-group">
							<label for="seleccion" class="col-lg-2 control-label">Nombre Alumno</label>
							<div class="col-lg-10">
								<select name="seleccion" class="form-control" placeholder="Selecciona alumno" title="Se necesita que selecciones un alumno" required>
								<?php
								$sql="SELECT DISTINCT cod_alumno,nombre_apellidos FROM alumnos";
								$resultado=mysql_query($sql,conexion());
								while($fila=mysql_fetch_array($resultado)){
								$cod_alumno=$fila[0];
								echo "<option value='".$cod_alumno."'>".$fila[1]."</option>";
								}?>
								<option value="*">Todos</option>
								</select>
							</div>
						  </div>
						  <div class="form-group">
							<label for="mensaje" class="col-lg-2 control-label">Mensaje</label>
							<div class="col-lg-10">
							<textarea rows="5" cols="90" type="text" name="mensaje" class="form-control"  placeholder="Introduce tu mensaje" title="Se necesita que insertes el mensaje"required></textarea>							
							</div>
						  </div>
						  <div class="form-group">
							<label for="fecha1" class="col-lg-2 control-label">Fecha desde</label>
							<div class="col-lg-10">
								<input name="fecha1" type="text" id="fecha" onClick="popUpCalendar(this, formulario.fecha, 'yyyy-mm-dd');" size="20"  class="form-control"  placeholder="Introduce fecha inicio" title="Se necesita que insertes fecha inicio"  required>						
							</div>
						  </div>
						  <div class="form-group">
							<label for="fecha2" class="col-lg-2 control-label">Fecha hasta</label>
							<div class="col-lg-10">
							<input name="fecha2" type="text" id="fecha1" onClick="popUpCalendar(this, formulario.fecha2, 'yyyy-mm-dd');" size="20" class="form-control"  placeholder="Introduce fecha final" title="Se necesita que insertes fecha final" required>							
							</div>
						  </div>
						  <div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
							  <button type="submit" name="insertar" class="btn btn-default" id="insertar">Enviar mensaje</button>
							</div>
						  </div>
						 
					</form>
				</section>
		</div>	
<?php
	mostrar_footer();
?>
