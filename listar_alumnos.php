﻿<?php 
	//Para controlar los errores de la base de datos.
	error_reporting(0);
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["profesor"])){
		header("location: index.php?nop");
	}
	//Obtenemos todos los alumnos
	$alumnos=obtener_alumnos();
	mostrar_header();
	mostrarmenu_profesor();
?>	
	<!--Container -->
			<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Listado de los alumnos</h1>
				<section>
				<?php
					//Listamos los profesores
					listar_alumnos($alumnos);
				?>
				</section>
			</div>
<?php
	mostrar_footer();
?>
