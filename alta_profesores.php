﻿<?php 
	//Para controlar los errores de la base de datos.
	error_reporting(0);
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["administrador"])){
		header("location: index.php?noad");
	}

	//Si apreta el boton de insertar
	if(isset($_GET['insertar'])){
						if($_GET['nombre']<>null)
						{
						if($_GET['identificacion']<>null)
						{
						if($_GET['clave']<>null)
						{
						if($_GET['ciclo']<>null)
						{
						if($_GET['telefono']<>null)
						{
						if($_GET['centro']<>null)
						{
						//LLamamos a la funcion insertar y le pasamos los valores recogidos en el formulario.
						if(insertar_profesores($_GET['nombre'],$_GET['identificacion'],$_GET['clave'],$_GET['ciclo'],$_GET['telefono'],$_GET['centro'])){
							$mensaje = "<span style='color:green'>Datos insertados con exito</span>";
						}
						else{
							$mensaje = "Error, no se ha podido insertar";
						}
						}//null centro
						}//null telefono
						}//null ciclo
						}//null clave
						}//null identificacion
						}// null nombre
	}
	mostrar_header();
	mostrarmenu_administrador();
	?>
	<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Alta profesores</h1>
				<section>
					<form class="form-horizontal" role="form" method="GET" action="alta_profesores.php">
						<input type="hidden" name="codigo">	
						<center><p style=color:red> <?php  echo $mensaje;?></p></center>
						  <div class="form-group">
							<label for="nombre" class="col-lg-2 control-label">Nombre y apellidos</label>
							<div class="col-lg-10">
							<input type="text" name="nombre" class="form-control"  placeholder="Introduce tu nombre y apellidos" title="Se necesita nombre y apellidos"required />							
							</div>
						  </div>
						  <div class="form-group">
							<label for="identificacion" class="col-lg-2 control-label">Identificacion</label>
							<div class="col-lg-10">
							  <input type="text" name="identificacion" class="form-control" id="identificacion" 
									 placeholder="Introduce identificacion" title="Se necesita identificacion"required />
							</div>
						  </div>
						  <div class="form-group">
							<label for="clave" class="col-lg-2 control-label">Clave</label>
							<div class="col-lg-10">
							  <input type="password" name="clave" class="form-control" id="clave" 
									 placeholder="Introduce clave" title="Se necesita clave"required />
							</div>
						  </div>
						  <div class="form-group">
							<label for="ciclo" class="col-lg-2 control-label">Ciclo</label>
							<div class="col-lg-10">
							  <input type="text" name="ciclo" class="form-control" id="ciclo" 
									 placeholder="Introduce ciclo" title="Se necesita ciclo" required />
							</div>
						  </div>
						  <div class="form-group">
							<label for="telefono" class="col-lg-2 control-label">Telefono</label>
							<div class="col-lg-10">
							  <input type="number" name="telefono" class="form-control" id="telefono" 
									 placeholder="Introduce telefono" title="Se necesita telefono"required />
							</div>
						  </div>
						  <div class="form-group">
							<label for="centro" class="col-lg-2 control-label">Centro</label>
							<div class="col-lg-10">
							  <input type="text" name="centro" class="form-control" id="centro" 
									 placeholder="Introduce centro" title="Se necesita centro"required />
							</div>
						  </div>
						  <div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
							  <button type="submit" name="insertar" class="btn btn-default" id="insertar">Insertar</button>
							</div>
						  </div>
					</form>
				</section>
			</div>
<?php
	mostrar_footer();
?>  
   