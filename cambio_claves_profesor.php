﻿<?php 
	include("BD.php");
	include("vistas.php");
	session_start();
	//Comprobamos si la sesion existe sino redirigimos a la pantalla de inicio.
	if(!isset($_SESSION["profesor"])){
		header("location: index.php?nop");
	}
	if(isset($_POST['modificar'])){
		$contrasenavieja=md5($_POST['contrasenavieja']);
		$contrasenanueva=$_POST['contrasenanueva'];
		$repcontrasena=$_POST['repcontrasena'];
		$nombreusuario=$_SESSION['profesor'];
		
		$sql="SELECT * FROM profesores WHERE identificacion=\"$nombreusuario\"";
		$resultado=mysql_query($sql,conexion());
		$fila=mysql_fetch_array($resultado);
		$passBD=$fila['clave'];
		
		if($contrasenavieja==$passBD)
		{
			if($contrasenanueva==$repcontrasena)
				{
				//Cambiamos la tabla del usuario con la contraseña nueva.
				if(cambio_claves_profesor($contrasenanueva,$nombreusuario))
				{
					$mensaje = "<span style='color:green'>Contraseña modificacion con exito</span>";
				}
				else{ $mensaje = "No se puede insertar:conectate con el administrador";}
				
			}else{
				$mensaje = "Las claves no coinciden";
			}
			
		}else{
			$mensaje = "Error en la contraseña actual";
		}
	}
	
	mostrar_header();
	mostrarmenu_profesor();
?>	
	<!--Container -->
			<div class="container" style="min-height:500px;">
				<br>
				<h1 id="encabezado">Cambio de claves</h1>
				<section>
					<form class="form-horizontal" role="form" method="post" action="cambio_claves_profesor.php">
						<input type="hidden" name="codigo"/>
						  <center><p style=color:red> <?php  echo $mensaje;?></p></center>
						  <div class="form-group">
							<label for="contrasenavieja" class="col-lg-2 control-label">Contraseña antigua</label>
							<div class="col-lg-10">
							<input type="password" name="contrasenavieja" class="form-control"  placeholder="Introduce contraseña antigua" title="Se necesita que insertes contraseña antigua" required></textarea>							
							</div>
						  </div>
						  <div class="form-group">
							<label for="contrasenanueva" class="col-lg-2 control-label">Contraseña nueva</label>
							<div class="col-lg-10">
							<input type="password" name="contrasenanueva" class="form-control"  placeholder="Introduce contraseña nueva" title="Se necesita que insertes la contraseña nueva" required></textarea>							
							</div>
						  </div>
						  <div class="form-group">
							<label for="repcontrasena" class="col-lg-2 control-label">Confirmar Contraseña</label>
							<div class="col-lg-10">
							<input type="password" name="repcontrasena" class="form-control"  placeholder="Introduce la repeticion de la contraseña" title="Se necesita que insertes la confirmacion de la contraseña" required></textarea>							
							</div>
						  </div>
						  <div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
							  <button type="submit" name="modificar" class="btn btn-default" id="modificar">Modificar</button>
							</div>
						  </div>
						 
					</form>
				</section>
		</div>	
<?php
	mostrar_footer();
?>
